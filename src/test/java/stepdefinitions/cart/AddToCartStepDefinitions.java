package stepdefinitions.cart;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.cart.AddToCartTests;

public class AddToCartStepDefinitions {
    WebDriver driver;
    AddToCartTests addToCartTests;

    public AddToCartStepDefinitions() {
        this.driver = DriverFactory.getDriver();
        addToCartTests = new AddToCartTests(driver);
    }

    @Then("I opened view of one product from study material tab")
    public void openedDetailViewOfOneProductFromStudyMaterialTab() throws InterruptedException {
        addToCartTests.openDetailedViewOfOneProduct();
    }

    @And("Increased quantity and added product to cart")
    public void increasedQuantityAndAddedProductToCart() {
        addToCartTests.setQuantityAndAddProductToCart();
    }

    @Then("Opened cart and checked the added products")
    public void openedCartAndCheckedTheAddedProducts() {
        addToCartTests.openCartAndValidateAddedProducts();
    }

    @And("Empty cart")
    public void emptyCart() {
        addToCartTests.emptyCart();
    }
}
