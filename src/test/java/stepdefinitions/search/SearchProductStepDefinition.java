package stepdefinitions.search;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import tests.search.SearchProductTests;

public class SearchProductStepDefinition {
    SearchProductTests searchProductTests;
    WebDriver driver;

    public SearchProductStepDefinition() {
        this.driver = DriverFactory.getDriver();
        searchProductTests = new SearchProductTests(driver);
    }

    @Given("Searched with keyword {string} and validated the fetched products category")
    public void searchedWithKeywordAndValidatedTheFetchedProductsCategory(String searchValue) {
        searchProductTests.searchAndValidateFetchedCategory(searchValue);
    }
}
