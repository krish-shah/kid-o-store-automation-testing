package stepdefinitions.login;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.login.LoginTests;

public class LoginStepDefinitions {
    WebDriver driver;
    LoginTests loginTests;

    public LoginStepDefinitions() {
        driver = DriverFactory.getDriver();
        loginTests = new LoginTests(driver);
    }

    @Given("I visited login page")
    public void iVisitedLoginPage() {
        loginTests.visitLoginPage();
    }

    @Given("I logged in with {string} username and {string} password")
    public void iLoggedInWith(String username, String password) {
        loginTests.enterUsernamePasswordAndSubmit(username, password);
    }

    @Then("I can see my profile icon")
    public void iCanSeeMyProfileIcon() {
        loginTests.validateSuccessfulLogin();
    }
}
