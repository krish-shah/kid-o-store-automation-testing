package tests.search;

import org.openqa.selenium.WebDriver;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.search.SearchModalValidation;
import validations.search.SearchResultValidation;

public class SearchProductTests {
    WebDriver driver;
    LoaderValidation loaderValidation;
    NavigationValidation navigationValidation;
    SearchResultValidation searchResultValidation;
    SearchModalValidation searchModalValidation;

    public SearchProductTests(WebDriver driver) {
        this.driver = driver;
        loaderValidation = new LoaderValidation(driver);
        navigationValidation = new NavigationValidation(driver);
        searchResultValidation = new SearchResultValidation(driver);
        searchModalValidation = new SearchModalValidation(driver);
    }

    public void searchAndValidateFetchedCategory(String searchValue) {
        loaderValidation.waitForLoaderToBecomeInvisible();
        navigationValidation.clickOnSearchIcon();
        searchModalValidation.search(searchValue);
        loaderValidation.waitForLoaderToBecomeInvisible();
        searchResultValidation.validateSearchResultCategory(searchValue);
    }
}
