package tests.cart;

import org.openqa.selenium.WebDriver;
import validations.cart.CartValidation;
import validations.home.exclusiveProducts.ExclusiveProductsValidation;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.product.SingleProductValidation;

import java.util.HashMap;

public class AddToCartTests {
    WebDriver driver;
    ExclusiveProductsValidation exclusiveProductsValidation;
    LoaderValidation loaderValidation;
    SingleProductValidation singleProductValidation;
    NavigationValidation navigationValidation;
    HashMap<String, Object> productData;
    CartValidation cartValidation;

    public AddToCartTests(WebDriver driver) {
        this.driver = driver;
        exclusiveProductsValidation = new ExclusiveProductsValidation(driver);
        loaderValidation = new LoaderValidation(driver);
        singleProductValidation = new SingleProductValidation(driver);
        navigationValidation = new NavigationValidation(driver);
        productData = new HashMap<>();
        cartValidation = new CartValidation(driver);
    }

    public void openDetailedViewOfOneProduct() throws InterruptedException {
        exclusiveProductsValidation.openRandomProduct();
    }

    public void setQuantityAndAddProductToCart() {
        loaderValidation.waitForLoaderToBecomeInvisible();
        singleProductValidation.setQuantity();
        singleProductValidation.storeProductData();
        singleProductValidation.clickOnAddToCartButton();
        singleProductValidation.validateSuccessfullyAddedToCartMessage();
        navigationValidation.goToCart();
    }

    public void openCartAndValidateAddedProducts() {
        productData = singleProductValidation.getProductData();
        String actualProductName = productData.get("name").toString();
        navigationValidation.goToCart();
        loaderValidation.waitForLoaderToBecomeInvisible();
        cartValidation.validateProductName(actualProductName);
        cartValidation.validateProductVariant(actualProductName, productData.get("variant").toString());
        cartValidation.validateProductPrice(actualProductName, (float)productData.get("price"));
        cartValidation.validateProductQuantity(actualProductName, (int)productData.get("quantity"));
        cartValidation.validateTotal(actualProductName, (float)productData.get("price"), (int)productData.get("quantity"));
    }

    public void emptyCart() {
        if(navigationValidation.getCartItemCount() > 0) {
            navigationValidation.goToCart();
            loaderValidation.waitForLoaderToBecomeInvisible();
            cartValidation.emptyCart();
            navigationValidation.goToHome();
        }
    }
}
