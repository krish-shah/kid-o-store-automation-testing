package tests.login;

import org.openqa.selenium.WebDriver;
import validations.loader.LoaderValidation;
import validations.login.LoginValidation;
import validations.navigation.NavigationValidation;

public class LoginTests {
    WebDriver driver;
    LoginValidation loginValidation;
    LoaderValidation loaderValidation;
    NavigationValidation navigationValidation;

    public LoginTests(WebDriver driver) {
        this.driver = driver;
        loginValidation = new LoginValidation(driver);
        loaderValidation = new LoaderValidation(driver);
        navigationValidation = new NavigationValidation(driver);
    }

    public void visitLoginPage() {
        loginValidation.visitLoginPage();
    }

    public void enterUsernamePasswordAndSubmit(String username, String password) {
        loaderValidation.waitForLoaderToBecomeInvisible();
        loginValidation.enterUsername(username);
        loginValidation.enterPassword(password);
        loginValidation.clickOnLoginButton();
    }

    public void validateSuccessfulLogin() {
        loaderValidation.waitForLoaderToBecomeInvisible();
        navigationValidation.validateLogoutButtonIsVisible();
    }
}
