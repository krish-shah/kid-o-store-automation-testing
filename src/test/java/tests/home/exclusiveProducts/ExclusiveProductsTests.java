package tests.home.exclusiveProducts;

import org.openqa.selenium.WebDriver;
import validations.home.exclusiveProducts.ExclusiveProductsValidation;
import validations.loader.LoaderValidation;

public class ExclusiveProductsTests {
    WebDriver driver;
    ExclusiveProductsValidation exclusiveProductsValidation;
    LoaderValidation loaderValidation;

    public ExclusiveProductsTests(WebDriver driver) {
        this.driver = driver;
        exclusiveProductsValidation = new ExclusiveProductsValidation(driver);
        loaderValidation = new LoaderValidation(driver);
    }

    public void openStudyMaterialTab() throws InterruptedException {
        loaderValidation.waitForLoaderToBecomeInvisible();
        exclusiveProductsValidation.clickOnStudyMaterialTab();
    }

    public void validateNumberOfProductsFeaturedOnStudyMaterialTab() {
        exclusiveProductsValidation.validateNumberOfProductsFeaturedOnStudyMaterialTab();
    }
}
