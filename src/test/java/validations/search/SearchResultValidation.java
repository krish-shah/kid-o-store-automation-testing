package validations.search;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import validations.search.constants.SearchResultConstants;

import java.util.List;

public class SearchResultValidation implements SearchResultConstants {
    WebDriver driver;

    public SearchResultValidation(WebDriver driver) {
        this.driver = driver;
    }

    public void validateSearchResultCategory(String searchValue) {
        List<WebElement> searchResultProductNames = driver.findElements(FETCHED_PRODUCT_NAMES);
        for (int i=0; i<searchResultProductNames.size(); i++) {
            System.out.println("Inside category validation");
            Assert.assertTrue(searchResultProductNames.get(i).getText().contains(searchValue));
        }
    }
}
