package validations.search;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.search.constants.SearchModalConstants;

import java.security.Key;
import java.time.Duration;

public class SearchModalValidation implements SearchModalConstants {
    WebDriver driver;
    WebDriverWait wait;

    public SearchModalValidation(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    public void search(String searchValue) {
        wait.until(ExpectedConditions.elementToBeClickable(SEARCH_INPUT));
        driver.findElement(SEARCH_INPUT).sendKeys(searchValue, Keys.ENTER);
    }
}
