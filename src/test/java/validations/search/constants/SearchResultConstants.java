package validations.search.constants;

import org.openqa.selenium.By;

public interface SearchResultConstants {
    By FETCHED_PRODUCT_NAMES = By.cssSelector(".shop_container .product_title a");
}
