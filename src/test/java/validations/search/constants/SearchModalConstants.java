package validations.search.constants;

import org.openqa.selenium.By;

public interface SearchModalConstants {
    By SEARCH_INPUT = By.id("search-input");
}
