package validations.navigation.constants;

import org.openqa.selenium.By;

public interface NavigationConstants {
    By USER_ICON = By.xpath("//i[@class='ti-user me-2']/parent::div/parent::li");
    By LOGOUT_BUTTON = By.xpath("//button[normalize-space()='Logout']");

    By CART_ICON = By.xpath("//li[@class='dropdown cart_dropdown'][1]");

    By VIEW_CART_BUTTON = By.xpath("//div[@id='header-cart-body']//div[@id='header-cart-footer']/p[@class='cart_buttons']/a[1]");
    String LOGOUT_BUTTON_NOT_VISIBLE_MESSAGE = "Either logout button is not visible or there is some problem in login";
    By CART_ITEM_COUNT = By.id("cart-count");
    By HOME = By.xpath("//div[@id='navbarSupportedContent']/ul/li/a[contains(text(), 'Home')]");
    By SEARCH_ICON = By.className("search_trigger");
}
