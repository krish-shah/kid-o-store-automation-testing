package validations.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import validations.cart.constants.CartConstants;

import java.util.List;

public class CartValidation implements CartConstants {
    WebDriver driver;
    public CartValidation(WebDriver driver) {
        this.driver = driver;
    }

    public void validateProductName(String actualProductName) {
        String extractedProductName = driver.findElement(By.xpath(PRODUCT_NAME.replace("productName", actualProductName))).getText();
        Assert.assertTrue(extractedProductName.equalsIgnoreCase(actualProductName));
    }

    public void validateProductVariant(String actualProductName, String actualVariant) {
        String extractedVariant = driver.findElement(By.xpath(PRODUCT_VARIANT.replace("productName", actualProductName))).getText().split(":")[1].trim();
        Assert.assertEquals(extractedVariant, actualVariant);
    }

    public void validateProductPrice(String actualProductName, float actualProductPrice) {
        String formattedProductPrice = PRODUCT_PRICE.replace("productName", actualProductName);
        float extractedProductPrice = Float.parseFloat(driver.findElement(By.xpath(formattedProductPrice)).getText());
        Assert.assertEquals(extractedProductPrice, actualProductPrice);
    }

    public void validateProductQuantity(String actualProductName, int actualQuantity) {
        String formattedProductQuantity = PRODUCT_QUANTITY.replace("productName", actualProductName);
        int extractedProductQuantity = Integer.parseInt(driver.findElement(By.xpath(formattedProductQuantity)).getAttribute("value"));
        Assert.assertEquals(extractedProductQuantity, actualQuantity);
    }

    public void validateTotal(String actualProductName, float actualPrice, int actualQuantity) {
        String formattedProductTotalXPath = PRODUCT_TOTAL.replace("productName", actualProductName);
        float calculatedTotal = actualPrice*(float)actualQuantity;
        float extractedTotal = Float.parseFloat(driver.findElement(By.xpath(formattedProductTotalXPath)).getText());
        Assert.assertEquals(extractedTotal, calculatedTotal);
    }

    public void emptyCart() {
        List<WebElement> removeItemButtons = driver.findElements(REMOVE_ITEM_BUTTON);
        for(int i=0; i<removeItemButtons.size(); i++)
            removeItemButtons.get(i).click();
    }
}
