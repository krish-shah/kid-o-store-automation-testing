package validations.cart.constants;

import org.openqa.selenium.By;

public interface CartConstants {
    String PRODUCT_NAME = "//tbody[@id='cart-table-body']/tr/td/a[contains(text(),'productName')]/parent::td/parent::tr/td[2]";
    String PRODUCT_VARIANT = "//tbody[@id='cart-table-body']/tr/td/a[contains(text(),'productName')]/parent::td/parent::tr/td[3]/div/div";
    String PRODUCT_PRICE = "//tbody[@id='cart-table-body']/tr/td/a[contains(text(),'productName')]/parent::td/parent::tr/td[4]/span[@class='price']";
    String PRODUCT_QUANTITY = "//tbody[@id='cart-table-body']/tr/td/a[contains(text(),'productName')]/parent::td/parent::tr/td[5]/div/input";
    String PRODUCT_TOTAL = "//tbody[@id='cart-table-body']/tr/td/a[contains(text(),'productName')]/parent::td/parent::tr/td[6]";
    By REMOVE_ITEM_BUTTON = By.xpath("//tbody[@id='cart-table-body']/tr/td[7]/a");
}
