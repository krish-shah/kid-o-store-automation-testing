package validations.loader.constants;

import org.openqa.selenium.By;

public interface LoaderConstants {
    By LOADER = By.cssSelector(".preloader");
}
