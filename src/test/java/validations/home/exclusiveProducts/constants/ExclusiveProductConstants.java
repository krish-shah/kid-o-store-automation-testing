package validations.home.exclusiveProducts.constants;

import org.openqa.selenium.By;

public interface ExclusiveProductConstants {
    By STUDY_MATERIAL_TAB = By.xpath("//a[@id='study-material-kit-tab']");
    By UNIFORMS_TAB = By.xpath("//a[@id='uniforms-tab']");
    By BOOKS_TAB = By.xpath("//a[@id='books-tab']");
    By NOTEBOOKS_TAB = By.xpath("//a[@id='notebooks-tab']");
    By STUDY_MATERIAL_FEATURED_PRODUCTS = By.xpath("//div[@id='study-material-kit']/div/div");

    String PRODUCT_CARD_VIEW_DETAILS_BUTTON = "(//div[@id='study-material-kit']//div[@class='product product-wrapper'])[numberToReplace]/div[@class='product_img']/div[@class='product_action_box']/ul/li/a";
    int NUMBER_OF_PRODUCTS_TO_FEATURE_ON_STUDY_MATERIAL_TAB = 8;
    String WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_STUDY_MATERIAL_TAB = "Wrong number of products featured on study material tab";
}
