package validations.home.exclusiveProducts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.DriverUtils;
import validations.home.exclusiveProducts.constants.ExclusiveProductConstants;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;

public class ExclusiveProductsValidation implements ExclusiveProductConstants{
    WebDriver driver;
    WebDriverWait wait;

    Actions actions;

    public ExclusiveProductsValidation(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        actions = new Actions(driver);
    }

    public void clickOnStudyMaterialTab() throws InterruptedException{
        WebElement studyMaterialTab = driver.findElement(STUDY_MATERIAL_TAB);
        DriverUtils.scrollElementToCenter(studyMaterialTab, driver);
        studyMaterialTab.click();
    }

    public void clickOnUniformsTab() throws InterruptedException{
        WebElement uniformsTab = driver.findElement(UNIFORMS_TAB);
        DriverUtils.scrollElementToCenter(uniformsTab, driver);
        uniformsTab.click();
    }

    public void clickOnBooksTab() throws InterruptedException{
        WebElement booksTab = driver.findElement(BOOKS_TAB);
        DriverUtils.scrollElementToCenter(booksTab, driver);
        booksTab.click();
    }

    public void clickOnNoteBooksTab() throws InterruptedException{
        WebElement noteBooksTab = driver.findElement(NOTEBOOKS_TAB);
        DriverUtils.scrollElementToCenter(noteBooksTab, driver);
        noteBooksTab.click();
    }

    public void validateNumberOfProductsFeaturedOnStudyMaterialTab() {
        int numberOfProductsFeatured = driver.findElements(STUDY_MATERIAL_FEATURED_PRODUCTS).size();
        Assert.assertEquals(numberOfProductsFeatured, NUMBER_OF_PRODUCTS_TO_FEATURE_ON_STUDY_MATERIAL_TAB, WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_STUDY_MATERIAL_TAB);
    }

    public void openRandomProduct() throws InterruptedException{
        int productCardNumber = ThreadLocalRandom.current().nextInt(1, (NUMBER_OF_PRODUCTS_TO_FEATURE_ON_STUDY_MATERIAL_TAB+1));
        String formattedProductCardViewDetailsButton = PRODUCT_CARD_VIEW_DETAILS_BUTTON.replace("numberToReplace", Integer.toString(productCardNumber));
        DriverUtils.scrollElementToCenter(driver.findElement(By.xpath(formattedProductCardViewDetailsButton)), driver);
        actions.moveToElement(driver.findElement(By.xpath(formattedProductCardViewDetailsButton)));
        actions.perform();
        driver.findElement(By.xpath(formattedProductCardViewDetailsButton)).click();
    }
}
