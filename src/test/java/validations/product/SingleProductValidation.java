package validations.product;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import validations.product.constants.SingleProductConstants;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class SingleProductValidation  implements SingleProductConstants {
    WebDriver driver;
    WebDriverWait wait;
    HashMap<String, Object> productData;

    public SingleProductValidation(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        productData = new HashMap<>();
    }

    public void storeProductData() {
        productData.put("name", extractName());
        productData.put("price", extractPrice());
        productData.put("variant", extractVariant());
        productData.put("quantity", extractQuantity());
    }

    public HashMap<String, Object> getProductData() {
        return productData;
    }

    public void setQuantity() {
        int quantity = ThreadLocalRandom.current().nextInt(1, 5);
        driver.findElement(QUANTITY_INPUT).clear();
        driver.findElement(QUANTITY_INPUT).sendKeys(quantity+"");
    }

    public void clickOnAddToCartButton() {
        driver.findElement(ADD_TO_CART_BUTTON).click();
    }

    public String extractName() {
        return driver.findElement(PRODUCT_NAME).getText();
    }

    public float extractPrice() {
        return Float.parseFloat(driver.findElement(PRODUCT_PRICE).getText());
    }

    public String extractVariant() {
        return driver.findElement(PRODUCT_VARIANT).getText();
    }

    public int extractQuantity() {
        return Integer.parseInt(driver.findElement(QUANTITY_INPUT).getAttribute("value"));
    }

    public void validateSuccessfullyAddedToCartMessage() {
        WebElement addedToCartToaster = wait.until(ExpectedConditions.visibilityOfElementLocated(ADDED_TO_CART_TOASTER));
        Assert.assertEquals(addedToCartToaster.getText(), ADDED_TO_CART_MESSAGE);
    }
}
