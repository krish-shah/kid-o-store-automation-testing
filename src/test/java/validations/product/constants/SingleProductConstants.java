package validations.product.constants;

import org.openqa.selenium.By;

public interface SingleProductConstants {
    By QUANTITY_INPUT = By.xpath("//input[@name='quantity']");
    By ADD_TO_CART_BUTTON = By.xpath("//button[normalize-space()='Add to cart']");
    By ADDED_TO_CART_TOASTER = By.xpath("//div[contains(text(), 'Successfully Added to cart')]");
    By PRODUCT_NAME = By.xpath("//h4[@class='product_title']/a");
    By PRODUCT_PRICE = By.xpath("//div[@class='product_price']/span");
    By PRODUCT_VARIANT = By.xpath("//div[contains(@class, 'product_size_switch')]/span[contains(@class, 'active')]");

    String ADDED_TO_CART_MESSAGE = "Successfully Added to cart!";
}
