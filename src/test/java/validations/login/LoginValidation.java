package validations.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.login.constants.LoginConstants;

import java.time.Duration;

public class LoginValidation implements LoginConstants{
    WebDriver driver;
    WebDriverWait wait;

    public LoginValidation(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    public void visitLoginPage() {
        driver.get(LOGIN_URL);
    }

    public void enterUsername(String username) {
        driver.findElement(USERNAME_INPUT).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
    }

    public void clickOnLoginButton() {
        driver.findElement(SUBMIT_BUTTON).click();
    }
}
