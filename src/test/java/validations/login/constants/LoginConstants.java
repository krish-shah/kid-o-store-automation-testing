package validations.login.constants;

import org.openqa.selenium.By;

public interface LoginConstants {
    String LOGIN_URL = "https://internal.kidostore.in/login";
    By USERNAME_INPUT = By.xpath("//input[@id='email']");
    By PASSWORD_INPUT = By.xpath("//input[@id='password']");
    By SUBMIT_BUTTON = By.xpath("//button[@id='btn-submit']");
}
