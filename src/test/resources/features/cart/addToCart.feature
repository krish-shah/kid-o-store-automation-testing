@AddToCart
Feature: Adding product to cart

  Background: User is on home page
    Given I visited login page
    Then I logged in with "user25@gmail.com" username and "abcd1234" password
    And Empty cart

  Scenario: Adding product to cart
    Given I opened study material
    Then I opened view of one product from study material tab
    And Increased quantity and added product to cart
    Then Opened cart and checked the added products