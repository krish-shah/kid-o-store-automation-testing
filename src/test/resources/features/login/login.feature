@Login
Feature: Login with user Account
    As a user i should be able to login

Background: User is on the login page
    Given I visited login page

Scenario: I logged in with valid credentials
    Given I logged in with "user16@gmail.com" username and "abcd1234" password
    Then I can see my profile icon