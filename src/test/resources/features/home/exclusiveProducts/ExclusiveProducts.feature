@ExclusiveProducts
  Feature: Checking exclusive products
    Checking tabs and components of featured products
  Background: User is on login page
    Given I visited login page
    Then I logged in with "user16@gmail.com" username and "abcd1234" password
    Scenario: Validating exclusive products of study material tab
      Given I opened study material
      Then Checked only required number of products are featured on study material tab
