package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class DriverFactory {
    //    public  static ThreadLocal<WebDrive> t1 = new ThreadLocal<>();
    public  static ThreadLocal<WebDriver> t1 = new ThreadLocal<>();
    public ChromeOptions options;

    public void initDriver() {
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        t1.set(new ChromeDriver(options));
        getDriver().manage().window().maximize();
    }

    public  static  WebDriver getDriver() {
        return t1.get();
    }
}
